<!doctype html>
<html lang="en">

<?php include '_partials/html-head.php'; ?>

<body class="home">
    <div id ="wrapper">
            <?php include '_partials/header.php'; ?>
            <div id="content" class="container">
                   <div class="columns twelve offset-by-two">
                       <?php include '_partials/search.php'; ?>
                   </div>
            </div>  

            <a href="#" class="login-link intro-video" data-toggle="modal" data-target="#video">
                <img src="http://i.vimeocdn.com/video/471193259_200x150.jpg" border="0" />
                <br/>
                Watch the introduction to <strong>nudle</strong>                
            </a>
            <div id="video" class="modal fade in">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <iframe src="//player.vimeo.com/video/91707281" width="555" height="320" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> 
                            </div>
                        </div>
                    </div>
            </div>
    </div>

    <?php include '_partials/footer.php'; ?>
</body>
</html>