<?php 
    // REviews
    include 'reviews.php';    
    
    $cwo_reviews = $reviews;
    krsort($cwo_reviews);

    $modules = array(
        'g53cwo' => array(
            'type' => 'module',
            'title' => 'Computers in the World',
            'masthead-img' => 'images/cwo-header.jpg',
            'avg-rating' => '7.4',
            'meta-details' => array(
                'module-code' => 'G53CWO',
                'level' => 'Level 3',
                'semester' => 'Autumn Semester 13/14',
                'credits' => '10 credits',
                'convener' => 'Convener: <strong>Dr. Dario Landa-Silva</strong>',
            ), 
            "ratings" => array(
                array("7.4", "Overall Rating"),
                array("8.1", "Asessments"),
                array("7.6", "Exercises"),
                array("7.0", "Content"),
                array("6.9", "Lectures")
            ),
            "reviews" => $cwo_reviews,
        )
    );

    function ordinal($a) {
      // return English ordinal number
      return $a.substr(date('jS', mktime(0,0,0,1,($a%10==0?9:($a%100>20?$a%10:$a%100)),2000)),-2);
    }
 ?>







