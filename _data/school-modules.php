<?php 
    $compsci_semesters = array(
        'autumn' => array(
            'title' => 'Autumn Semester',
            'levels' => array(
                1 => array(
                    // level, module-code, title, rating
                    array( "G51APS", "Algorithmic Problem Solving", "6.2"),
                    array( "G51CSA", "Computer Systems Architecture", "8.3"),
                    array( "G51MCS", "Mathematics for Computer Scientists", "7.0"),
                    array( "G51PRG", "Introduction to Programming", "9.2"),
                    array( "G51REQ", "Introduction to Requirements Engineering", "3.6"),
                    array( "G51UST", "Unix and Software Tools", "5.2"),
    
                ),
                2 => array(
                    array( "G52ADS", "Algorithms and Data Structures", "8.3"),
                    array( "G52APR", "Application Programming", "8.8"),
                    array( "G52GUI", "Graphical User Interfaces", "7.0"),
                    array( "G52IFR", "Introduction to Formal Reasoning", "9.2"),
                    array( "G52IIP", "Introduction to Image Processing", "5.5"),
                    array( "G52PAS", "Planning and Search", "6.9"),
                    array( "G52SEM", "Software Engineering Methodologies", "8.0"),
                ), 
                3 => array(
                    array( "G53ARS", "Autonomous Robotic Systems", "8.5"),
                    array( "G53CCT", "Collaboration and Communication Technologies", "7.2"),
                    array( "G53CMP", "Compilers", "6.8"),
                    array( "G53CWO", "Computers in the World", "6.5"),
                    array( "G53KRR", "Knowledge Representation and Reasoning", "7.0"),
                    array( "G53SQM", "Software Quality Management", "3.2")
                ), 
                4 => array(
                    array( "G54AAD", "Advanced Algorithms and Data Structures", "6.3"),
                    array( "G54ADM", "System and Network Administration  ", "2.7"),
                    array( "G54CCS", "Connected Computing at Scale   ", "5.6"),
                    array( "G54FOP", "Mathematical Foundations of Programming", "7.5"),
                    array( "G54FPP", "Foundations of Programming Mini-Project", "4.4"),
                    array( "G54GRP", "Horizon DTC Group Project  ", "9.1"),
                    array( "G54IHC", "Introduction to Human Computer Interaction ", "8.2"),
                    array( "G54MET", "Methods for Understanding Users in Computer Science", "3.0"),
                    array( "G54ORM", "Operations Research and Modelling  ", "3.6"),
                    array( "G54PRG", "Programming", "2.3"),
                    array( "G54UBI", "Ubiquitous Computing   ", "7.1"),
                    array( "G54VIS", "Computer Vision (Level 4)  ", "8.5"),
                    array( "G64AAD", "Advanced Algorithms and Data Structures", "6.0"),
                    array( "G64DBS", "Database Systems   ", "9.2"),
                    array( "G64ICP", "Introduction to Computer Programming   ", "5.7"),
                    array( "G64INC", "Introduction to Network Communications ", "4.4"),
                    array( "G64IPD", "Introduction to Computer Programming and Developmen", "2.6")
                )
            ),
        ), 
        'spring' => array(
                    'title' => 'Spring Semester',
                    'levels' => array(
                        1 => array(
                            // level, module-code, title, rating
                            array( "G51APS", "Algorithmic Problem Solving", "6.2"),
                            array( "G51CSA", "Computer Systems Architecture", "8.3"),
                            array( "G51MCS", "Mathematics for Computer Scientists", "7.0"),
                            array( "G51PRG", "Introduction to Programming", "9.2"),
                            array( "G51REQ", "Introduction to Requirements Engineering", "3.6"),
                            array( "G51UST", "Unix and Software Tools", "5.2"),
            
                        ),
                        2 => array(
                            array( "G52ADS", "Algorithms and Data Structures", "8.3"),
                            array( "G52APR", "Application Programming", "8.8"),
                            array( "G52GUI", "Graphical User Interfaces", "7.0"),
                            array( "G52IFR", "Introduction to Formal Reasoning", "9.2"),
                            array( "G52IIP", "Introduction to Image Processing", "5.5"),
                            array( "G52PAS", "Planning and Search", "6.9"),
                            array( "G52SEM", "Software Engineering Methodologies", "8.0"),
                        ), 
                        3 => array(
                            array( "G53ARS", "Autonomous Robotic Systems", "8.5"),
                            array( "G53CCT", "Collaboration and Communication Technologies", "7.2"),
                            array( "G53CMP", "Compilers", "6.8"),
                            array( "G53CWO", "Computers in the World", "6.5"),
                            array( "G53KRR", "Knowledge Representation and Reasoning", "7.0"),
                            array( "G53SQM", "Software Quality Management", "3.2")
                        ), 
                        4 => array(
                            array( "G54AAD", "Advanced Algorithms and Data Structures", "6.3"),
                            array( "G54ADM", "System and Network Administration  ", "2.7"),
                            array( "G54CCS", "Connected Computing at Scale   ", "5.6"),
                            array( "G54FOP", "Mathematical Foundations of Programming", "7.5"),
                            array( "G54FPP", "Foundations of Programming Mini-Project", "4.4"),
                            array( "G54GRP", "Horizon DTC Group Project  ", "9.1"),
                            array( "G54IHC", "Introduction to Human Computer Interaction ", "8.2"),
                            array( "G54MET", "Methods for Understanding Users in Computer Science", "3.0"),
                            array( "G54ORM", "Operations Research and Modelling  ", "3.6"),
                            array( "G54PRG", "Programming", "2.3"),
                            array( "G54UBI", "Ubiquitous Computing   ", "7.1"),
                            array( "G54VIS", "Computer Vision (Level 4)  ", "8.5"),
                            array( "G64AAD", "Advanced Algorithms and Data Structures", "6.0"),
                            array( "G64DBS", "Database Systems   ", "9.2"),
                            array( "G64ICP", "Introduction to Computer Programming   ", "5.7"),
                            array( "G64INC", "Introduction to Network Communications ", "4.4"),
                            array( "G64IPD", "Introduction to Computer Programming and Developmen", "2.6")
                        )
                    ),
                ),
            'full-year' => array(
                    'title' => 'Full Year Semester',
                    'levels' => array(
                        1 => array(
                            // level, module-code, title, rating
                            array( "G51APS", "Algorithmic Problem Solving", "6.2"),
                            array( "G51CSA", "Computer Systems Architecture", "8.3"),
                            array( "G51MCS", "Mathematics for Computer Scientists", "7.0"),
                            array( "G51PRG", "Introduction to Programming", "9.2"),
                            array( "G51REQ", "Introduction to Requirements Engineering", "3.6"),
                            array( "G51UST", "Unix and Software Tools", "5.2"),
            
                        ),
                        2 => array(
                            array( "G52ADS", "Algorithms and Data Structures", "8.3"),
                            array( "G52APR", "Application Programming", "8.8"),
                            array( "G52GUI", "Graphical User Interfaces", "7.0"),
                            array( "G52IFR", "Introduction to Formal Reasoning", "9.2"),
                            array( "G52IIP", "Introduction to Image Processing", "5.5"),
                            array( "G52PAS", "Planning and Search", "6.9"),
                            array( "G52SEM", "Software Engineering Methodologies", "8.0"),
                        ), 
                        3 => array(
                            array( "G53ARS", "Autonomous Robotic Systems", "8.5"),
                            array( "G53CCT", "Collaboration and Communication Technologies", "7.2"),
                            array( "G53CMP", "Compilers", "6.8"),
                            array( "G53CWO", "Computers in the World", "6.5"),
                            array( "G53KRR", "Knowledge Representation and Reasoning", "7.0"),
                            array( "G53SQM", "Software Quality Management", "3.2")
                        ), 
                        4 => array(
                            array( "G54AAD", "Advanced Algorithms and Data Structures", "6.3"),
                            array( "G54ADM", "System and Network Administration  ", "2.7"),
                            array( "G54CCS", "Connected Computing at Scale   ", "5.6"),
                            array( "G54FOP", "Mathematical Foundations of Programming", "7.5"),
                            array( "G54FPP", "Foundations of Programming Mini-Project", "4.4"),
                            array( "G54GRP", "Horizon DTC Group Project  ", "9.1"),
                            array( "G54IHC", "Introduction to Human Computer Interaction ", "8.2"),
                            array( "G54MET", "Methods for Understanding Users in Computer Science", "3.0"),
                            array( "G54ORM", "Operations Research and Modelling  ", "3.6"),
                            array( "G54PRG", "Programming", "2.3"),
                            array( "G54UBI", "Ubiquitous Computing   ", "7.1"),
                            array( "G54VIS", "Computer Vision (Level 4)  ", "8.5"),
                            array( "G64AAD", "Advanced Algorithms and Data Structures", "6.0"),
                            array( "G64DBS", "Database Systems   ", "9.2"),
                            array( "G64ICP", "Introduction to Computer Programming   ", "5.7"),
                            array( "G64INC", "Introduction to Network Communications ", "4.4"),
                            array( "G64IPD", "Introduction to Computer Programming and Developmen", "2.6")
                        )
                    ),
                )
    );

    $schools = array(
        'compsci' => array(
            'type' => 'school',
            'title' => 'Computer Science',
            'masthead-img' => 'images/students.png',
            'meta-details' => array(
                'year' => 'Academic Year: <strong>2013/2014</strong>',
            ),
            'semesters' => $compsci_semesters
        )
    );
 ?>