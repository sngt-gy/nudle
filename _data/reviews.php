<?php 
    function randomDate() {
        $date_min = 1351915245;
        $date_max = 1393819245;
        $date = mt_rand($date_min, $date_max);
        return $date;
    }

    function formatDate($d) {
        $date = date("F jS, Y",$d);
        return $date;
    }

    $reviews = array(
        // type, Year of study, Course, Title, Text, Rating
        randomDate() => array(
            "text", "3", "Computer Science Bsc", "I Learnt a lot ", 
            "The coursework (which make up 25% of the module) were at times quite difficult, however due to the fantastic tutors that are there to help, with a bit of time, it is not hard to get good marks for them. It is quite a difficult module to do, however it is definitely worthwhile as it teaches vital information that is used in many other modules.",
            "6.9"
        ),
        randomDate() => array(
            "text", "3", "Computer Science and Business MSci", "Time Well Spent", 
            "CWO was a great module, taught by a great lecturer. Dario was very approachable and happily answered any questions that I had. The course works were interesting, and actually quite enjoyable to do! Unfortunately the lack of peer assessment ruined them for some groups as it meant a lot of people weren't bothering to turn up. I learnt a lot from this module and it will definitely help me as I pursue my future career in computing.",
            "8.2"
        ),
        randomDate() => array(
            "text", "3", "Computer Science MSci", "Great Module", 
            "Even though it’s a compulsory module I’m really happy i had the chance to take this module. Definitely one of the best modules in computer science. The module was laid out in a very organised manner. We had weekly assignments as well which meant we were always kept up to date with course material and after model answers were placed in moodle so we could see how we could improve our next assignment. Definitely one of the best modules in computer science.",
            "9.2"
        ),
        randomDate() => array(
            "text", "3", "Computer Science and Artificial Intelligence MSci", "Interesting, But Difficult", 
            "It was an interesting subject to take, however some of the concepts behind it were quite hard to grasp.I think the lectures needed to be explained better and found myself questioning what was happening far too often. The course works were tough at first, but if you spent time on them, they became clearer and weren't as bad as they initially seemed. It was definitely worthwhile taking and I feel I have a good understanding of how compilers work now.",
            "4.2"
        ),
        randomDate() => array(
            "text", "2", "Computer Science and Mathematics MSci ", "Venanzio Capretta Was A Refreshing Change", 
            "Venanzio, the lecturer, was a refreshing change. He was a lot more interesting than the majority of the lecturers that I have previously had and his clear enjoyment of the subject was infectious. The coursework were well planned and weren’t too difficult if you did your research before attempting them and the exam was a fair reflection of the module. I thoroughly enjoyed the module and could not recommend it more.",
            "9.5"
        ),

        // 0     1               2      3      4            5       6
        // type, Year of study, Course, Title, Video embed, Rating, Img thumb
        randomDate() => array(
            "video", "3", "Computer Science Bsc", "g53cwo review ", 
            '<iframe src="//player.vimeo.com/video/91816422" width="555" height="320" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>',
            "7.0",
            "http://i.vimeocdn.com/video/471336583_200x150.jpg"
        ),
        randomDate() => array(
            "video", "3", "Computer Science Bsc", "CWO Review", 
            '<iframe src="//player.vimeo.com/video/91678475" width="555" height="320" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>',
            "7.5",
            "http://i.vimeocdn.com/video/471154004_200x150.jpg"
        ),
        // 0     1               2      3      4          5       
        // type, Year of study, Course, Title, Image, Rating
        randomDate() => array(
            "image", "3", "Computer Science Bsc", "Afternoon delight", 
            '../images/cwo-mike-dario.jpg',
            "8.2"
        ), 
        randomDate() => array(
            "image", "3", "Computer Science Bsc", "Boring lecture ",
            "../images/cr2.GIF",
            "7.1"
        ),
        randomDate() => array(
            "image", "3", "Computer Science Bsc", "Another Ethics video ",
            "../images/cs.jpg",
            "7.4"
        ),
        randomDate() => array(
            "image", "3", "Computer Science Bsc", "Attendance in a CWO lecture",
            "../images/cs2.jpg",
            "7.7"
        ),
        randomDate() => array(
            "image", "3", "Computer Science Bsc", "Fundamentals of Ethics",
            "../images/dooley.jpg",
            "8.5"
        ),
        randomDate() => array(
            "image", "3", "Computer Science Bsc", "We love this lecture ",
            "../images/green.jpg",
            "8.8"
        ),
        randomDate() => array(
            "image", "3", "Computer Science Bsc", "Best lecture by far",
            "../images/lt.jpg",
            "8.2"
        ),
        randomDate() => array(
            "image", "3", "Computer Science Bsc", "CWO lecture with 20th Century Teaching",
            "../images/lt2.jpg",
            "6.7"
        ),
        randomDate() => array(
            "image", "3", "Computer Science Bsc", "Guest lecture on ethics",
            "../images/lt3.jpg",
            "6.1"
        ),
        // randomDate() => array(
        //     "image", "3", "Computer Science Bsc", "Afternoon Delight",
        //     "../images/lt4.jpg",
        //     "7.0"
        // )

    );
?>