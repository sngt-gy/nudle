 $(function() {


    $('#share-modal').on('shown.bs.modal' , function(e){
        var aslider = new Dragdealer('a-rating-slider', {
            steps: 10,
            snap: true,
            animationCallback: function(x, y) {
                console.log("Hello");
                $('#a-rating-slider .value').text(Math.round(x * 10));
            }
        });



        var bslider = new Dragdealer('b-rating-slider', {
            steps: 10,
            snap: true,
            animationCallback: function(x, y) {
                console.log("Hello");
                $('#b-rating-slider .value').text(Math.round(x * 10));
            }
        });


        var cslider = new Dragdealer('c-rating-slider', {
            steps: 10,
            snap: true,
            animationCallback: function(x, y) {
                console.log("Hello");
                $('#c-rating-slider .value').text(Math.round(x * 10));
            }
        });

        var dslider = new Dragdealer('d-rating-slider', {
            steps: 10,
            snap: true,
            animationCallback: function(x, y) {
                console.log("Hello");
                $('#d-rating-slider .value').text(Math.round(x * 10));
            }
        });
    });

    //Sticky headers for semesters
    if($('#semesters').length > 0) {
        var btm = 0;
        if($('.sem-full-year .sem-title').length > 0) {
            btm = 28;
        }
        $('.sem-autumn .sem-title').scrollToFixed( {bottom: 0, limit: $('.sem-autumn .sem-title').offset().top});
        $('.sem-spring .sem-title').scrollToFixed( {bottom: btm, limit: $('.sem-spring .sem-title').offset().top});
        $('.sem-full-year .sem-title').scrollToFixed( {bottom: 0, limit: $('.sem-full-year .sem-title').offset().top});
    }

    //Remove default behaviours 
    $('a.share-experience, a.login-link').on('click', function(e){
        e.preventDefault();
    }); 


    //MODULE INFORMATION TOGGLE
    $('#module-info-toggle').on('click', function(e){
        e.preventDefault();
        var className = 'less-info';
        if( $('#mod-info').hasClass(className) ) {
            $('#mod-info').removeClass(className);
            $('#module-info-toggle span.text').html("Less Information");
        } else {
            $('#mod-info').addClass(className);
            $('#module-info-toggle span.text').html("More Information");
        }
    });


    // RATING COLORS 
     $('.rating').each(function(){
        var rawVal = $('.rating-value', this).html();
        var value = parseFloat(rawVal);

        var ratingClass = "";

        if(value > 7.0) {
            ratingClass = "rating-high";
        } else if(value >= 4.0 && value <=7.0) {
            ratingClass = "rating-medium";
        } else {
            ratingClass = "rating-low";
        }
        console.log("Rating", ratingClass);
        $(this).addClass(ratingClass);
     });


    // MODAL TRIGGERS
    $('.video-thumb a, .image-thumb a').on('click', function(e){
        e.preventDefault();
        elem = e.target;
        targetElem = $(e.target).parent().next();
        $(targetElem).modal('show');
    });


 });
