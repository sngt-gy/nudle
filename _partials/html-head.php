<head> 
    <meta charset="utf-8">
    <title>nudle - University of Nottingham Module Catalogue</title>
    <meta name="description" content="nudle - University of Nottingham Module Catalogue">
    <meta name="author" content="NMD - Group 14">

    <link href='http://fonts.googleapis.com/css?family=Signika:400,600,700,300' rel='stylesheet' type='text/css'>
    
    <link rel="stylesheet" type="text/css" href="<?php print $DIR_PREFIX; ?>css/normalize.css">
    <link rel="stylesheet" type="text/css" href="<?php print $DIR_PREFIX; ?>css/skeleton-fluid.css">
    <link rel="stylesheet" type="text/css" href="<?php print $DIR_PREFIX; ?>css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php print $DIR_PREFIX; ?>css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="<?php print $DIR_PREFIX; ?>css/dragdealer.css">
    <link rel="stylesheet" href="<?php print $DIR_PREFIX; ?>css/main.css">

    
    <script type="text/javascript" src="<?php print $DIR_PREFIX; ?>js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="<?php print $DIR_PREFIX; ?>js/jquery-ui-1.10.4.custom.min.js"></script>
    <script type="text/javascript" src="<?php print $DIR_PREFIX; ?>js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php print $DIR_PREFIX; ?>js/jquery-scrolltofixed-min.js"></script>
    <script type="text/javascript" src="<?php print $DIR_PREFIX; ?>js/dragdealer.js"></script>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>