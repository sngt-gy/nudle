<div id="header">
    <div class="container">
        <div class="nav-header nav-header-left columns five">
            <ul>
                <li class="nav-link-search"><a href="/nudle">Search</a></li>
                <li class="nav-link-browse"><a href="/nudle/browse">Browse Catalogue</a></li>
            </ul>
        </div>

        <div id="logo" class="columns six">
            <a href="/nudle">
                <h1>nudle</h1>
                <span class="subtitle">
                    University of Nottingham Module Catalogue
                </span>
            </a>
        </div>

        <div class="nav-header nav-header-right columns five">
            <ul>
                <li><a class="login-link" href="#" >Login/Register</a></li>
            </ul>
            
        </div>

        <?php include 'login-register.php'; ?>
    </div>
</div>