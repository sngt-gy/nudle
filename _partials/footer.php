<script type="text/javascript" src="<?php print $DIR_PREFIX; ?>js/nudle.js"></script>

<div id="footer">
    <div class="container">
        <div class="nav-footer columns twelve">
            <ul>
                <li class="nav-link-search"><a href="/nudle">Search</a></li>
                <li class="nav-link-browse"><a href="/nudle/browse">Browse Catalogue</a></li>
            </ul>
            <div class="credits">
                Brought to you by New Media Design - Group 14 <br/>
                <em>Alex H Edwards, Ben Ross, Jorge Loaiza, Michael Tawafig, Samy Nakayama Driss, Sangeet Gyawali </em>
            </div>  

        </div>
        <div class="notts-logo columns four">
            <img src="<?php print $DIR_PREFIX; ?>images/notts.png" border="0">
        </div>
    </div>
</div>