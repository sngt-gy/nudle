<?php $DIR_PREFIX = "../" ?>
<?php include '../_data/school-modules.php'; ?>
<?php 
    $school = $schools['compsci']; 
    $sems = $school['semesters'];
?>

<!doctype html>

<html lang="en">
<?php include $DIR_PREFIX .'_partials/html-head.php'; ?>

<body class="school">
    <div id ="wrapper">
        <?php include '../_partials/header.php'; ?>
        <div id="masthead" style="background-image:url('<?php print $DIR_PREFIX.$school['masthead-img']; ?>')">
            <div class="overlay">&nbsp;</div>
            <div class="container">
                <div class="card columns twelve">
                    <h2><?php print $school['title']; ?></h2>
                    <ul class="meta">
                        <?php foreach ($school['meta-details'] as $key => $value): ?>
                            <li class="meta-detail meta-<?php print $key ?>">
                                <?php print $value; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>

        <div id="semesters">
            <?php foreach ($sems as $key => $data): ?>
                <div id="sem-<?php print $key; ?>" class="sem sem-<?php print $key; ?>">
                    <div class="sem-title">
                        <a href="#sem-<?php print $key; ?>"><?php print $data['title']; ?></a>
                        
                    </div>
                    
                    <div class="container">
                        <div class="legend">
                             <div class="columns two offset-by-two">Code</div>
                            <div class="columns ten">Module Title</div>
                            <div class="columns two avg-rating">Overall Rating</div>
                        </div>
                    <?php foreach ($data['levels'] as $key => $modules): ?>
                            <div class="sem-level">
                            <div class="columns two">
                                <h4>Level <?php print $key; ?></h4>
                            </div>
                            

                            <div class="sem-modules columns fourteen">
                                <ul>
                                    <?php foreach ($modules as $value): ?>
                                        <li>
                                            <div class="module-field module-code columns two">
                                                <?php print $value[0]; ?>
                                            </div>
                                            <div class="module-field  module-title columns twelve">
                                                <a href="/nudle/module">    
                                                    <?php print $value[1] ?>
                                                </a>
                                            </div>
                                            <div class="module-field columns two">
                                                <div class="rating rating-small float-right">
                                                    <div class="rating-value">
                                                        <?php print $value[2] ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    <?php endforeach; ?>    
                                       
                                 </ul>
                            </div>
                        </div>

                    

                    <?php endforeach; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <?php include '../_partials/footer.php'; ?>
</body>
</html>