<div id="share-modal" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Share your experience</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <h4>Module Rating</h4>  
                        <div class="description">Rate the quality of the module content: </div>
                        <div id="a-rating-slider" class="dragdealer">
                          <div class="handle">Assesments <span class="value">0</span></div>
                        </div>
                        <div id="b-rating-slider" class="dragdealer">
                          <div class="handle">Excercises <span class="value">0</span></div>
                        </div>
                        <div id="c-rating-slider" class="dragdealer">
                          <div class="handle">Content <span class="value">0</span></div>
                        </div>
                        <div id="d-rating-slider" class="dragdealer">
                          <div class="handle">Lectures <span class="value">0</span></div>
                        </div>

                    </div>


                    <ul class="nav nav-tabs clear-both" >
                        <li class="active"><a href="#text" data-toggle="tab">Text</a></li>
                        <li><a href="#video" data-toggle="tab">Video</a></li>
                        <li><a href="#image" data-toggle="tab">Image</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="text">
                            <label>Title:</label>
                            <input type="text">
                            <label>Your review:</label>
                            <textarea rows="4"></textarea>
                            
                        </div>                    
                        <div class="tab-pane fade" id="video">
                            <label>Title:</label>
                                <input type="text">
                            <label>Video Link:</label>
                            <input type="text" placeholder="e.g. youtube, vimeo video link,">
                        </div>                    
                        <div class="tab-pane fade" id="image">
                        <label>Title:</label>
                                <input type="text">
                            <label>Choose image:</label>
                            <input type="File">
                        </div>                    
                    </div>

                </form>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-lg" data-dismiss="modal">Submit</button>
            </div>
        </div>
    </div>
</div>  

    
</div>