
<?php 
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
?>
<?php $DIR_PREFIX = "../" ?>
<?php include '../_data/module-details.php'; ?>
<?php $mod = $modules['g53cwo']; ?>

<!doctype html>

<html lang="en">
<?php include $DIR_PREFIX .'_partials/html-head.php'; ?>

<body class="module">
    <div id ="wrapper">
		<?php include '../_partials/header.php'; ?>

		<div id="masthead" style="background-image:url('<?php print $DIR_PREFIX.$mod['masthead-img']; ?>')">
			<div class="overlay">&nbsp;</div>
			<div class="container">
				<div class="card columns twelve">
					<h2><?php print $mod['title'] ?></h2>
					<ul class="meta">
						<?php foreach ($mod['meta-details'] as $key => $value): ?>
							<li class="meta-detail meta-<?php print $key ?>">
								<?php print $value; ?>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
				<div class="columns four">
					<div class="rating rating-large">
						<div class="rating-value"><?php print $mod['avg-rating']; ?></div>
						<span class="rating-label">Overall Rating</span>
					</div>
				</div>
			</div>
		</div>

		

		<div id="mod-info" class="less-info">
			<div class="container">
				<div class="columns seven">
					<div class="element">
						<h3>Offering School:<span> Computer Science </span></h3>
					</div>
					
					<div class="element items-spaced">
						<h3>Target Students</h3>
						<p>Part I undergraduate students in the School of Computer Science </p>
						<p>Available to students from other Schools with the agreement of the module convenor.</p>
						<p>Available to JYA/Erasmus students.</p>
						<p>This module is part of the Artificial Intelligence theme in the School of Computer Science.</p>
					</div>	
					
					<div class="element">
						<h3>Pre-Requisites:</h3>
						<table>
							<tr>
								<td class="module-code">G51PRG</td>
								<td>Introduction to Programming</td>
							</tr>
						</table>
						<p class="additional-info">Or equivalent programming experience and knowledge of mathematics.</p>
					</div>

					<div class="element">
						<h3>Summary of Content</h3>
						<p>This module introduces the field of digital image processing, a fundamental component of digital photography, television, computer graphics and computer vision.
						You’ll cover topics including: image processing and its applications; fundamentals of digital images; digital image processing theory and practice; and applications of image processing. 
						You’ll spend around three hours in lectures and computer classes each week for this module.
						</p>
					</div>	
				</div>

				<div class="columns nine">
					
					
					<div class="element">
						<h3>Teaching Method & Frequency:</h3>
						<table>
							<tr>
								<td>
									<strong>Lecture</strong>
								</td>
								<td>
									11 Weeks
								</td>
								<td>
									2 per week
								</td>
								<td>
									2 hours long
								</td>
							</tr>			
							<tr>
								<td>
								<strong>Computing</strong>
								</td>
								<td>
								11 Weeks
								</td>
								<td>
								1 per week
								</td>
								<td>
								2 hours long
								</td>
							</tr>
						</table>
						<p>Activities may take place every teaching week of the Semester or only in specified weeks. It is usually specified above if an activity only takes place in some weeks of a Semester
						</p>
					</div>	
					
					<div class="element">
						<h3>Assessment:</h3>
						<table>
							<tr>
								<td>
								<strong>Exam 1</strong>
								</td>
								<td>
								1 hour written examination
								</td>
								<td>
								60%
								</td>
							</tr>
							<tr>
								<td>
								<strong>Coursework 1</strong>
								</td>
								<td>
								2000 word programming assignment/report
								</td>
								<td>
								40%
								</td>
							</tr>
						</table>
					</div>	
					<div class="element">
						<h3>Education Aims</h3>
						<p>To introduce the fundamentals of digital image processing theory and practice. To gain practical experience in writing programs for manipulating digital images. To lay the foundation for studying advanced topics in related fields.
						</p>
					</div>	
					<div class="element">
						<h3>Learning Outcomes:</h3>
						<p><strong>Knowledge and understanding:</strong> Experience implementing programs that manipulate images; understanding fundamental techniques in image processing and analysis, and their limitations; appreciation of the underlying mathematical principles of the field. 
						</p>
						<p>
						<strong>Intellectual Skills:</strong> Apply knowledge of image processing techniques to particular tasks; evaluate different techniques in the context of image manipulation and processing. 
						</p>
						<p>
						<strong>Professional Skills:</strong> Evaluate the applicability of various algorithms and operators to particular tasks. 
						</p>
						<p>
						<strong>Transferable Skills:</strong> Address real problems and assess the value of their proposed solutions, retrieve and analyse information from a variety of sources and produced detailed written reports on the result.
						</p>
					</div>	
				</div>

				
			</div>
			<a id="module-info-toggle" href="#">
				<div class="container">
					<div class="columns sixteen">
						<span class="text">Show More Information</span>
					</div>
				</div>
			</a>
		</div>




		
		<div id="mod-reviews">
			<div class="container">
				<div class="element columns sixteen">
					<h3>Student Experiences</h3>
				</div>
				<div id="ratings-matrix" class="columns sixteen alpha omega">
					<?php foreach ($mod['ratings'] as $key => $value): ?>
						<div class="columns two">
							<div class="rating rating-large">
								<div class="rating-value"><?php print $value[0] ?></div>
								<div class="rating-label"><?php print $value[1] ?></div>
							</div>
						</div>
					<?php endforeach; ?>
					<div class="columns two offset-by-four">
						<a class="share-experience float-right" href=""
							data-toggle="modal" data-target="#share-modal">
							<div class="share-button rating-large">
								<div class="rating-value">+</div>
								<div class="rating-label">Share Expreience</div>
							</div>
						</a>
					</div>
				</div>
			</div>
			<div class="reviews-matrix clear-both">
				<div class="container">
					<ul>
						<?php $count = 0;
							foreach ($mod['reviews'] as $date => $data):
								$class = ($count%4 == 0) ? " clear-both item-row-starter" : "";
						?>
							<li class="columns four <?php print $class; ?> ">
								<?php if ($data[0] == 'text'): ?>
									<?php include 'review-text.php'; ?>
								<?php elseif ($data[0] == 'image'): ?>
									<?php include 'review-image.php'; ?>
								<?php else: ?>
									<?php include 'review-video.php'; ?>
								<?php endif ?>
							</li>
						<?php $count++; endforeach; ?>
					</ul>
				</div> 

			</div>

			<div class="pagination">
				<div class="container">
					<ul>
						<li class="page-label">Pages: </li>
						<?php for ($i=1; $i < 3; $i++) : ?>
							<li class="page-item page-item-<?php print $i; ?>"><a href="#"><?php print $i; ?></a></li>
						<?php endfor; ?>
					</ul>
				</div>
			</div>

		</div>
		<?php include 'share.php';  ?>
    </div>

	 <?php include '../_partials/footer.php'; ?>
</body>
</html>