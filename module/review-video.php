<?php 
// 0     1               2      3      4           5       6
// type, Year of study, Course, Title, Video link, Rating, Img thumb
?>

<div class="review-item review-item-<?php print $data[0]; ?>">
    <div class="review-title">
        <h4><?php print $data[3]; ?></h4>
    </div>
    <div class="review-content">
        <div class="video-thumb">
            <a href="#" data-toggle="modal-media-frame"
                class="review-video-thumb" style="background-image: url(<?php print $data[6]; ?>)">&nbsp;</a>
        </div>
        <div class="video-frame modal modal-media-frame">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4><?php print $data[3]; ?></h4>
                    </div>
                    <div class="modal-body">
                        <?php print $data[4]; ?>            
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <div class="rating review-rating">
        <div class="rating-value">
            <?php print $data[5] ?>
        </div>
    </div>
    <div class="review-meta">
        <div class="meta-year"><strong><?php print ordinal($data[1]); ?> Year Student</strong></div>
        <div class="meta-course"><?php print $data[2] ?></div>
        <div class="meta-date"><?php print formatDate($date); ?></div>
    </div>
</div>