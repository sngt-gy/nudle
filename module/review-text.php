<?php 
//text review index   0     1              2       3             4               5
//           $data = [type, year of study, course, review title, review content, overall rating ]; 
?>

<div class="review-item review-item-<?php print $data[0]; ?>">
    <div class="review-title">
        <h4><?php print $data[3]; ?></h4>
    </div>
    <div class="review-content">
        <?php print $data[4]; ?>
    </div>
    <div class="rating review-rating">
        <div class="rating-value">
            <?php print $data[5] ?>
        </div>
    </div>
    <div class="review-meta">
        <div class="meta-year"><strong><?php print ordinal($data[1]); ?> Year Student</strong></div>
        <div class="meta-course"><?php print $data[2] ?></div>
        <div class="meta-date"><?php print formatDate($date); ?></div>
    </div>
</div>