<?php 
// 0     1               2      3      4          5       
// type, Year of study, Course, Title, Image, Rating
?>

<div class="review-item review-item-<?php print $data[0]; ?>">
    <div class="review-title">
        <h4><?php print $data[3]; ?></h4>
    </div>
    <div class="review-content">
        <div class="image-thumb">
            <a href="#" data-toggle="modal-media-frame"
                class="review-image-thumb" style="background-image: url(<?php print $data[4]; ?>)">&nbsp;</a>
        </div>
        <div class="image-modal modal modal-media-frame">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4><?php print $data[3]; ?></h4>
                    </div>
                    <div class="modal-body">
                        <img src="<?php print $data[4]; ?>" border=0>
                    </div>
                </div>
            </div>
                    
        </div>

    </div>
    <div class="rating review-rating">
        <div class="rating-value">
            <?php print $data[5] ?>
        </div>
    </div>
    <div class="review-meta">
        <div class="meta-year"><strong><?php print ordinal($data[1]); ?> Year Student</strong></div>
        <div class="meta-course"><?php print $data[2] ?></div>
        <div class="meta-date"><?php print formatDate($date); ?></div>
    </div>
</div>