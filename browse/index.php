<?php 
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
?>
<?php $DIR_PREFIX = "../" ?>
<?php include '../_data/schools.php'; ?>
<?php 
    $depts = $schools_depts; 
?>

<!doctype html>

<html lang="en">
<?php include $DIR_PREFIX .'_partials/html-head.php'; ?>

<body class="depts">
    <div id ="wrapper">
        <?php include '../_partials/header.php'; ?>
        <div id="masthead" style="background-image:url('<?php print $DIR_PREFIX.$depts['masthead-img']; ?>')">
            <div class="overlay">&nbsp;</div>
            <div class="container">
                <div class="card columns twelve">
                    <h2>Browse Catalogue</h2>
                </div>
            </div>
        </div>

        <div id="depts">                  
            <div class="container">
                <div class="depts-list columns sixteen">
                        <?php foreach ($depts['depts'] as $value): ?>
                                <div class="depts-item depts-title columns eight alpha omega">
                                    <a href="../course/"><?php print $value; ?></a>
                                </div>
                        <?php endforeach; ?>    
                           
                </div>
            </div>

        </div>

        <?php include '../_partials/footer.php'; ?>
</body>
</html>